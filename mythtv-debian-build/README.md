# build mythtv and mythplugins from source #

### mythtv-debian-build.sh: [1] ###

* git clone https://github.com/MythTV/ansible        #get ansible playbook to install required dependencies

* git clone https://github.com/MythTV/mythtv.git     #get current mythtv/plugins source

* sudo ansible-playbook -i hosts qt5.yml             #install dependencies

* sudo git checkout fixes/31                         # check out desired mythtv branch

* ./configure, make, install                         # build and install mythtv/mythplugins

[[1] https://www.mythtv.org/wiki/Build_from_Source#Installing_Build_Dependencies_without_Ansible](https://www.mythtv.org/wiki/Build_from_Source#Installing_Build_Dependencies_without_Ansible)

note: 

* < BLDIR=/home/user/build >    # build directory
* < make -j 5 >                 # -j jobs maybe better < -j -l 2 >   # [2]

[[2] https://www.scivision.dev/gnu-make-parallel-build/](https://www.scivision.dev/gnu-make-parallel-build/)

### also ###
