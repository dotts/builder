#!/bin/bash
## =================================
## this script will:
##   git clone https://github.com/MythTV/ansible       #get ansible playbook to install required dependencies
##   git clone https://github.com/MythTV/mythtv.git    #get current mythtv/plugins source
##   sudo ansible-playbook -i hosts qt5.yml            #install dependencies
##   sudo git checkout fixes/31                        # check out desired mythtv branch
##   configure, make, and install                      # build and install mythtv/mythplugins
## note:
##  < BLDIR=/home/user/build >                         # build directory
##  < make -j 5 >                                      # -j jobs maybe better < -j -l 2 > # [2]
## 
## [1] https://www.mythtv.org/wiki/Build_from_Source#Installing_Build_Dependencies_without_Ansible
## [2] https://www.scivision.dev/gnu-make-parallel-build/
## =================================
set -u # Variables that are not set are errors
set -x # Print commands when executed
#### trap read debug  # require a RETURN after each command executed

BLDIR=/home/user/build
export BLDIR
echo "$BLDIR"
mkdir "$BLDIR"
sudo mount /dev/sda1 "$BLDIR"

sudo apt-get install git ansible
cd $"$BLDIR"
git clone https://github.com/MythTV/ansible
git clone https://github.com/MythTV/mythtv.git

cd $"$BLDIR"/ansible
time sudo ansible-playbook -i hosts qt5.yml

cd $"$BLDIR"/mythtv
sudo git checkout fixes/31
sudo git clean -xfd

cd $"$BLDIR"/mythtv/mythtv
time ./configure
time make -j 5
sudo make install

cd $"$BLDIR"/mythtv/mythplugins
time ./configure
time make -j 5
sudo make install


## [1] https://get.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-10.8.0-amd64-standard.iso
## [2] https://github.com/MythTV/ansible
## [3] https://www.mythtv.org/wiki/Build_from_Source#Installing_Build_Dependencies_without_Ansible
## [4] 
## [] 
## [] 
## [] 
## [] 
## [] 
## [] https://cdimage.debian.org/cdimage/weekly-builds/amd64/iso-cd/debian-testing-amd64-netinst.iso
##
## sudo apt update && sudo apt install openssh-server && sudo service ssh start      # systemctl status ssh
## -- on host:  sftp -P 2468 user@localhost  or  ssh -p 2468 user@localhost
## install git/ansible 
## in build directory run ansible-playbook to get mythtv-build dependencies installed [2]
## build/install mythtv [3]
## build/install mythtv [3]
## 
## on host:
## downoad debian 10 iso to ~/virts/deb-myth-bld/ and run with 
##  < bash ../run-qemu.sh debian-live-10.8.0-amd64-standard.iso > [1]
##
## on guest:
##-- < sudo apt update && sudo apt install openssh-server git && sudo service ssh start>                       
##
## on host:
##-- < ssh -p 2468 user@localhost                                                    # default pw user:live
##
## on guest (ssh):    ## or just run this script < bash /home/user/builder/mythtv-debian-build.sh >
## 