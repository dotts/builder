echo ''
echo ''
echo 'after deb starts, on guest:'
echo ''
echo 'sudo apt update && sudo apt install openssh-server tmux git mc && sudo service ssh start'
echo 'git clone https://meherenow@bitbucket.org/dotts/builder.git'
echo ''
echo ''
echo 'on host:'
echo ''
echo 'ssh -p 2468 user@localhost'
echo ''

qemu-system-x86_64 \
-enable-kvm \
-m 8192 \
-cpu max \
-vga virtio \
-smp 4 \
-nic user,hostfwd=tcp::2468-:22 \
-boot menu=on \
-cdrom $1 \
mythtv-debian-build.qcow2 

## TODO add serial console
## -display spice-app \
#-cdrom ~/Downloads/manjaro-i3-20.2-minimal-201209-linux59.iso
