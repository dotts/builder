#!/bin/bash
## virtual alpine+nix
##
## in /home/wcl/builder/alpine-nix-build

echo 'commmand to mount floppy is in clipboard'

MK_IMG_FRMT='qcow2'
MK_IMG_FNAME='alpinix.qcow2'
MK_IMG_SIZE='10G'

DL_ISO_NAME='https://dl-cdn.alpinelinux.org/alpine/v3.16/releases/x86_64/alpine-virt-3.16.2-x86_64.iso'
DL_ISO_SHA256=$DL_ISO_NAME.sha256

##

#if [ ! -f $(basename $MK_IMG_FNAME) ]
#then
	cp $MK_IMG_FNAME{,.bak}
	qemu-img create -f $MK_IMG_FRMT $MK_IMG_FNAME $MK_IMG_SIZE
#fi

##

if [ ! -f $(basename $DL_ISO_NAME) ]
then	
	wget $DL_ISO_NAME
fi

##

if [ ! -f $(basename $DL_ISO_SHA256) ]
then	
	wget $DL_ISO_SHA256
fi

##


## push 'mount -t ext2 /dev/fd0 /media/floppy/' (and sda? mkdir/mount) onto clipboard
echo "mount -t ext2 /dev/fd0 /media/floppy/ && cd /media/floppy && ls"|xclip -sel c

qemu-system-x86_64 -enable-kvm -cdrom $DL_ISO_NAME -nographic -m 8192  -display gtk -fda /home/wcl/builder/alpine-nix-build/share/floppy-share.img -hda alpinix.qcow2 -boot menu=on


#### printf 'http://dl-cdn.alpinelinux.org/alpine/edge/main\nhttp://dl-cdn.alpinelinux.org/alpine/edge/community\nhttp://dl-cdn.alpinelinux.org/alpine/edge/testing\n' > /etc/apk/repositories && apk update

#### apk add openssh curl xz tmux util-linux sshfs mount neovim nano mc nnn links bat tree sudo ncdu dateutils bc sc-im e2fsprogs partclone git procps

#### [] https://wiki.alpinelinux.org/wiki/How_to_get_regular_stuff_working
## apk add util-linux pciutils usbutils coreutils binutils findutils grep iproute2
## apk add bash bash-doc bash-completion    ## chsh
## apk add udisks2 udisks2-doc    ## udisksctl status
## apk add build-base gcc abuild binutils binutils-doc gcc-doc
## apk add cmake cmake-doc extra-cmake-modules extra-cmake-modules-doc
